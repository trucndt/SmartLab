#include "RfidHandler.h"

using namespace RestClient;

RfidHandler::RfidHandler(Aggregator *aAggregator)
{
    mAggregator = aAggregator;
    mServPort = gPROG_ARGUMENT["rfidPort"].as<int>();
}

void RfidHandler::start()
{
    boost::thread tServer(&RfidHandler::server, this);
}

void RfidHandler::server()
{
    int servSock;					/* Socket descriptor for server */
    int clntSock;					/* Socket descriptor for client */
    struct sockaddr_in servAddr; 	/* Local address */
    struct sockaddr_in clntAddr; 	/* Client address */
    unsigned int clntLen;			/* Length of client address data structure */

    /* Create socket for incoming connections */
    if ((servSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
    {
        dieWithError("socket() failed\n");
    }

    /* Construct local address structure */
    memset(&servAddr, 0, sizeof(servAddr));		/* Zero out structure */
    servAddr.sin_family = AF_INET;				/* Internet address family */
    servAddr.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
    servAddr.sin_port = htons(mServPort);	  	/* Local port */

    const int Enable = 1;
    if (setsockopt(servSock, SOL_SOCKET, SO_REUSEADDR, &Enable, sizeof(int)) < 0)
    {
        dieWithError("setsockopt(SO_REUSEADDR) failed");
    }

    /* Bind to the local address */
    if (bind(servSock, (struct sockaddr *) &servAddr, sizeof(servAddr)) < 0)
    {
        dieWithError("RfidHandler bind() failed");
    }

    /* Mark the socket so it will listen for incoming connections */
    if (listen(servSock, MAXPENDING) < 0)
    {
        dieWithError("listen() failed");
    }

    while (1)
    {
        /* Set the size of the in-out parameter */
        clntLen = sizeof(clntAddr);

        /* Wait for a client to connect */
        cout << "Waiting for device ..." << endl;
        if ((clntSock = accept(servSock, (struct sockaddr *) &clntAddr,
                               &clntLen)) < 0)
        {
            cout << "accept() failed" <<endl;
            continue;
        }

        /* ClntSock is connected to a client! */
        string ClientAddr = inet_ntoa(clntAddr.sin_addr);

        cout << "Handling client " << ClientAddr << endl;

        /* Start new thread */
        boost::thread tDevice(&RfidHandler::deviceHandler, this, clntSock);
    }
}

void RfidHandler::deviceHandler(int aSock)
{
    int rcvMsgSize;
    char rcvBuffer[1024];

    while (1)
    {
        if ((rcvMsgSize = recv(aSock, rcvBuffer, 1024, 0)) <= 0)
        {
            cout << rcvMsgSize << "Err: recv() failed " << __FILE__ << ":" << __LINE__ << endl;
            close(aSock);
            return;
        }

        rcvBuffer[rcvMsgSize] = NULL;

        cout << "Received: " << rcvBuffer << endl;

        Json::Reader reader;
        Json::Value root;

        if (reader.parse(rcvBuffer, rcvBuffer + rcvMsgSize, root) == 0)
        {
            cout << "Error: Parse JSON failed" << endl;
            continue;
        }

        mAggregator->updUserEntranceToDB(root);
    }
}
