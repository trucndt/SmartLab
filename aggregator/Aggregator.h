#ifndef AGGREGATOR_H
#define AGGREGATOR_H

#include <boost/thread.hpp>
#include <map>
#include <string>
#include <boost/program_options.hpp>
#include <iostream>
#include "RfidHandler.h"
#include "HeartBeatServer.h"
#include "utils.h"

#include <json/json.h>

#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <restclient-cpp/restclient.h>
#include <restclient-cpp/connection.h>

using namespace std;
using namespace boost::program_options;

extern variables_map gPROG_ARGUMENT;

class RfidHandler;
class HeartBeatServer;
class HeartBeat;

class Aggregator
{
public:
    Aggregator();
    void start();

    /* Heart beat management method */

    /**
     * @brief setPcTimeStamp Set timestamp on Heartbeat value
     * @param aPCID The ID of PC
     * @param aTimeStamp The current time in miliseconds
     * @return 0 if success
     */
    int setPcTimeStamp(int aPCID, uint64_t aTimeStamp);

    /**
     * @brief checkPcStatus check if the PC is active
     * @param aPCID The ID of the PC
     * @return true if changing from active to inactive
     */
    bool checkPcStatus(int aPCID);

    /**
     * Get current status of a PC
     * @param aPCID the ID of the PC
     * @return true if active, false otherwise
     */
    bool getPcStatus(int aPCID);

    /* database communication method */

    /**
     * Update User entrance to DB
     * @param aRoot a Json containing user information
     */
    void updUserEntranceToDB(const Json::Value &aRoot);

private:
    static const uint8_t NUM_OF_PC = 20;

    vector<HeartBeat*> mHeartBeatValue;
    RfidHandler* mRfidHandler;
    HeartBeatServer* mHeartBeatServer;
    uint32_t mAliveThreshold;

    /** Handle database connection */
    RestClient::Connection* mDatabaseConn;
    boost::mutex mMtx_DatabaseConn;

    /**
     * Initialize mDatabaseConn
     * @return 0 if success
     */
    int initDatabaseConnection();

    /**
     * Update PC status to Database
     * @param aStt The new status
     * @param aPCID The ID of PC
     */
    void updPcSttToDB(int aPCID, bool aStt);

    /**
     * Post values to database
     * @param aPostUrl Url to append to the base Url
     * @param aValue a Json containing values
     * @return http code
     */
    int postDatabase(const string &aPostUrl, const Json::Value &aValue);

    /**
     * Put values to database
     * @param aPutUrl Url to append to the base Url
     * @param aValue a Json containing values
     * @return http code
     */
    int updateDatabase(const string &aPutUrl, const Json::Value &aValue);

    /**
     * Get values from database
     * @param aGetUrl Url to append to the base Url
     * @param retValue a Json containing returned values
     * @return http code
     */
    int queryDatabase(const string &aGetUrl, Json::Value &retValue);
};

class HeartBeat
{
private:
    uint64_t mTimeStamp;
    bool isActive;

public:
    HeartBeat()
    {
        mTimeStamp = 0;
        isActive = false;
    }

    uint64_t timeStamp() const;
    void setTimeStamp(const uint64_t &timeStamp);
    bool getIsActive() const;
    void getIsActive(bool value);
};

#endif // AGGREGATOR_H
