/*
 * main.cpp
 *
 *  Created on: Mar 2, 2016
 *      Author: trucndt
 */

#include "Aggregator.h"

using namespace std;
using namespace boost::program_options;

variables_map gPROG_ARGUMENT;

int processCommandLineArgument(int argc, char **argv);

int main(int argc, char **argv)
{
	if (processCommandLineArgument(argc, argv) < 0)
	{
		return 0;
	}

	cout << "Built time: " << __DATE__ << " " << __TIME__ << endl;

    Aggregator aggregator;
    aggregator.start();

    for (;;)
    {
        pause();
    }

	return 0;
}

int processCommandLineArgument(int argc, char **argv)
{
    options_description usage("Usage");

	usage.add_options()
            ("help,h", "Print help messages")
            ("heartBeatPort,p", value<string>()->default_value("12345"), "heart beat server port")
			("rfidPort", value<int>()->default_value(54321), "RFID server port")
            ("aliveThreshold", value<int>()->default_value(15000), "Heartbeat alive threshold");
	try
	{
        store(command_line_parser(argc, argv).options(usage).run(), gPROG_ARGUMENT);

		if (gPROG_ARGUMENT.count("help"))
		{
			cout << usage << endl;
			return -1;
		}

        notify(gPROG_ARGUMENT);
	}
    catch (required_option& e)
	{
		cout << "ERROR: " << e.what() << endl;
		cout << usage << endl;
		return -1;
	}
    catch (error& e)
	{
		cout << "ERROR: " << e.what() << endl;
		return -1;
	}

	return 0;
}
