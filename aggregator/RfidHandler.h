#ifndef RFIDHANDLER_H
#define RFIDHANDLER_H

#include "Aggregator.h"

class Aggregator;

class RfidHandler
{
public:
    RfidHandler(Aggregator *aAggregator);

    /**
     * Create server thread and exit
     */
    void start();

private:
    Aggregator* mAggregator;
    static const int MAXPENDING = 2;

    /** Listen port of RFID server */
    uint16_t mServPort;

    /**
     * Create a server listening for incoming connection
     */
    void server();

    /**
     * Handle the connection, receive user info and query database
     * @param aSock Socket of client
     */
    void deviceHandler(int aSock);
};

#endif // RFIDHANDLER_H
