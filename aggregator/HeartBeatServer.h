#ifndef HEARTBEATSERVER_H
#define HEARTBEATSERVER_H

#include "Aggregator.h"

class Aggregator;

class HeartBeatServer
{
public:
    HeartBeatServer(Aggregator* anAggregator);
    void start();

private:
    Aggregator* mAggregator;

    std::string mServPort;
    int servSocket;
    static const uint8_t NUM_OF_PC = 20;

    void hbHandler();
    void hbUpdater();
    void createUDPSocket();
    void *getRemoteAddrInfo(struct sockaddr *sa);

    /**
     * @brief
     * @param
     * @return
     */
    void printRemoteInfo(struct sockaddr_storage);
    void printPcStatus();
};

#endif // HEARTBEATSERVER_H
