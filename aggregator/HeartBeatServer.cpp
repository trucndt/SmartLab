#include "HeartBeatServer.h"

HeartBeatServer::HeartBeatServer(Aggregator *anAggregator)
{
    mAggregator = anAggregator;
    mServPort = gPROG_ARGUMENT["heartBeatPort"].as<string>();
}

void HeartBeatServer::start()
{
    boost::thread tHbHandler(&HeartBeatServer::hbHandler, this);
    boost::thread tHbUpdater(&HeartBeatServer::hbUpdater, this);
}

void HeartBeatServer::hbUpdater()
{
	cout << "Start updater.\n";
	while (1)
	{
		bool isActive = false;
		for (int i = 1; i <= NUM_OF_PC; i++)
		{
			isActive |= mAggregator->checkPcStatus(i);
		}

		if (isActive != false)
		{
			cout << "Update PCs' status.\n";
			printPcStatus();
		}

		usleep((gPROG_ARGUMENT["aliveThreshold"].as<int>() * 1000) / 5);
	}
}

void HeartBeatServer::hbHandler()
{
	int bytesRead = 0, pcID;
	char buffer[5];
	struct sockaddr_storage remote_addr;
	socklen_t addr_len;
	createUDPSocket();
	cout << "Waiting for HeartBeatClients...\n";

	while(1)
	{
		memset(buffer, 0, sizeof buffer);
		if ((bytesRead = recvfrom(servSocket, buffer, 5, 0, (struct sockaddr *)&remote_addr, &addr_len)) < 0)
		{
			dieWithError("hbHandler: recvfrom.\n");
		}

		/* Handle UDP packets */
		cout << "UDP packet recv: " << buffer << endl;
		pcID = atoi(buffer + 2);

		if (pcID >= 1 && pcID <= 20)
		{
			mAggregator->setPcTimeStamp(pcID, getCurrentTimeInMillis());
		}
	}
}

void HeartBeatServer::createUDPSocket()
{
    struct addrinfo hints, *servinfo, *p;
    int rv;

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC; // set to AF_INET to force IPv4
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = AI_PASSIVE; // use my IP

    cout << "Creating UDP socket...\n";

    if ((rv = getaddrinfo(NULL, mServPort.c_str(), &hints, &servinfo)) != 0)
    {
        dieWithError("createUDPSocket: getaddrinfo.\n");
        return;
    }

    // loop through all the results and bind to the first we can
    for(p = servinfo; p != NULL; p = p->ai_next)
    {
        if ((servSocket = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1)
        {
            dieWithError("createUDPSocket: socket.\n");
            continue;
        }

        if (bind(servSocket, p->ai_addr, p->ai_addrlen) == -1)
        {
            close(servSocket);
            dieWithError("createUDPSocket: bind.\n");
            continue;
        }

        break;
    }

    if (p == NULL)
    {
        dieWithError("createUDPSocket: failed to bind socket\n");
        return;
    }

    freeaddrinfo(servinfo);
}

void *HeartBeatServer::getRemoteAddrInfo(sockaddr *sa)
{
	if (sa->sa_family == AF_INET)
	{
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

void HeartBeatServer::printRemoteInfo(struct sockaddr_storage remote_addr)
{
	char s[INET6_ADDRSTRLEN];
	cout << "Receive UDP packet from " << inet_ntop(remote_addr.ss_family, getRemoteAddrInfo((struct sockaddr *)&remote_addr), s, sizeof s) << endl;
}

void HeartBeatServer::printPcStatus()
{
	struct tm *ptm;
	struct timeval tv;
	char time[40];

	gettimeofday(&tv, NULL);
	ptm = localtime(&tv.tv_sec);
	strftime (time, sizeof time, "%Y-%m-%d %H:%M:%S", ptm);

	cout << "Current time: " << time << endl;
	cout << "Online PC(s):\n";
	for (int i = 1; i <= NUM_OF_PC; i++)
	{
		if (mAggregator->getPcStatus(i))
		{
			string pad = (i < 10)? "   0": "   ";
			cout << pad << i;
		}
	}
	cout << endl;
}


