#include "Aggregator.h"
#include <boost/lexical_cast.hpp>

using namespace RestClient;

Aggregator::Aggregator()
{
    mHeartBeatServer = new HeartBeatServer(this);
    mAliveThreshold = gPROG_ARGUMENT["aliveThreshold"].as<int>();
    mRfidHandler = new RfidHandler(this);

    /* Initialize heartbeat structure */
    mHeartBeatValue.resize(NUM_OF_PC + 1);
    for (int i = 1; i <= NUM_OF_PC; i++)
    {
        mHeartBeatValue[i] = new HeartBeat();
    }

    // Initialize RestClient
    initDatabaseConnection();
}

int Aggregator::initDatabaseConnection()
{
    init();
    mDatabaseConn = new Connection("https://api.backand.com/");
    mDatabaseConn->SetTimeout(5);

    HeaderFields header;

    // set headers
    HeaderFields headers;
    headers["Accept"] = "application/json";
    headers["Content-Type"] = "application/json";
    headers["AnonymousToken"] = "1b153920-9915-420b-a1c3-97e38e233884";
    mDatabaseConn->SetHeaders(headers);

    // Initialize PC status on database
    cout << "Initializing PC status on database ...\n";

    Json::Value root;
    root["status"] = false;

    Json::FastWriter writer;
    string stt = writer.write(root);

    for (int i = 1; i <= 20; i++)
    {
        mDatabaseConn->put("/1/objects/pcList/" + boost::lexical_cast<string>(i), stt);
    }

    return 0;
}

void Aggregator::start()
{
    mHeartBeatServer->start();
    mRfidHandler->start();
}

int Aggregator::setPcTimeStamp(int aPCID, uint64_t aTimeStamp)
{
    bool curStatus = mHeartBeatValue[aPCID]->getIsActive();

    mHeartBeatValue[aPCID]->setTimeStamp(aTimeStamp);

    bool newStatus = (aTimeStamp + mAliveThreshold > getCurrentTimeInMillis())? true : false;

    if (newStatus != curStatus)
    {
        mHeartBeatValue[aPCID]->getIsActive(newStatus);

        /* Update database */
        boost::thread tDB(&Aggregator::updPcSttToDB, this, aPCID, newStatus);
    }

    return 0;
}

bool Aggregator::checkPcStatus(int aPCID)
{
    bool curStatus = mHeartBeatValue[aPCID]->getIsActive();

    if (curStatus && mHeartBeatValue[aPCID]->timeStamp() + mAliveThreshold < getCurrentTimeInMillis())
    {
        mHeartBeatValue[aPCID]->getIsActive(false);

        /* Update database */
        boost::thread tDB(&Aggregator::updPcSttToDB, this, aPCID, false);

        return true;
    }

    return false;
}

int Aggregator::updateDatabase(const string &aPutUrl, const Json::Value &aValue)
{
    Json::FastWriter writer;

    Response res = mDatabaseConn->put(aPutUrl, writer.write(aValue));

    return res.code;
}

int Aggregator::queryDatabase(const string &aGetUrl, Json::Value &retValue)
{
    Json::FastWriter writer;
    Json::Reader reader;

    Response res = mDatabaseConn->get(aGetUrl);
    reader.parse(res.body, retValue, false);

    return res.code;
}

bool Aggregator::getPcStatus(int aPCID)
{
	return mHeartBeatValue[aPCID]->getIsActive();
}

void Aggregator::updPcSttToDB(int aPCID, bool aStt)
{
    boost::lock_guard<boost::mutex> guard(mMtx_DatabaseConn);

    Json::Value root;
    root["status"] = aStt;

    //TODO Handle timeout and errors
    updateDatabase("/1/objects/pcList/" + boost::lexical_cast<string>(aPCID), root);
}

void Aggregator::updUserEntranceToDB(const Json::Value &aRoot)
{
    Json::Value root;
    Json::Value timeRoot;

    /*filter*/
    root["fieldName"] = "studentId";
    root["operator"] = "equals";
    root["value"] = aRoot["ID"];

    cout << "filter " << root << endl;

    Json::Value studentDB;
    Json::FastWriter writer;
    writer.omitEndingLineFeed();
    string geturl = string("/1/objects/student?pageSize=20&pageNumber=1&filter=").append(writer.write(root));
    queryDatabase(geturl, studentDB);
    string id = studentDB["data"][0]["id"].asString();

    struct tm *now;
    struct timeval tv;
    char timeBuf[100];
    gettimeofday(&tv, NULL);
    tv.tv_sec -= 7*3600;
    now = localtime(&tv.tv_sec);
    strftime(timeBuf, sizeof timeBuf, "%Y-%m-%dT%H:%M:%S.342Z", now);

    timeRoot["time"] = timeBuf;
    timeRoot["parentId"] = id;
    cout << timeRoot << endl;

    postDatabase("/1/objects/inTime", timeRoot);
}

int Aggregator::postDatabase(const string &aPostUrl, const Json::Value &aValue)
{
    Json::FastWriter writer;
    writer.omitEndingLineFeed();
    string outMsg = writer.write(aValue);

    cout << "Posting to database: " << outMsg << endl;

    Response r = mDatabaseConn->post(aPostUrl, outMsg);

    cout << r.body << endl;
    cout << r.code << endl;

    return 0;
}

bool HeartBeat::getIsActive() const
{
    return isActive;
}

void HeartBeat::getIsActive(bool value)
{
    isActive = value;
}

uint64_t HeartBeat::timeStamp() const
{
    return mTimeStamp;
}

void HeartBeat::setTimeStamp(const uint64_t &timeStamp)
{
    mTimeStamp = timeStamp;
}
