Dependencies:
  - Boost >= 1.63.0
  - CMake >= 3.7.2
  - libcurl

Directory structure:
  - aggregator/ : Source code for SmartLab
  - restclient-cpp/ : C++ REST API

To compile:
  Execute these commands:
    $ cd SmartLab
    $ mkdir build
    $ cd build
    $ cmake ..
    $ make

CMake structure
SmartLab/
├── CMakeLists.txt
├── aggregator
│   ├── CMakeLists.txt
├── restclient-cpp
│   ├── CMakeLists.txt
└── README.md
